CREATE SCHEMA supermercado;
USE supermercado;

CREATE TABLE Administrador(
	IdAdmin INTEGER NOT NULL PRIMARY KEY,
    NombreAdmin VARCHAR(100) NOT NULL,
    UserAdmin VARCHAR(100) NOT NULL,
    UserPassword VARCHAR(100) NOT NULL
);

CREATE TABLE Categoria(
	IdCategoria INTEGER NOT NULL PRIMARY KEY,
    NombreCategoria VARCHAR(100) NOT NULL,
    DescripcionCategoria VARCHAR(300),
    EstadoCategoria VARCHAR(20),
    IdAdmin INTEGER NOT NULL,
    FOREIGN KEY(IdAdmin) REFERENCES Administrador(IdAdmin)
);

CREATE TABLE Producto(
	IdProducto INTEGER NOT NULL PRIMARY KEY,
    NombreProducto VARCHAR(100) NOT NULL,
    DescripcionProducto VARCHAR(300),
    FotoProducto VARCHAR(100),
    PrecioProducto double NOT NULL,
    DescuentoProducto double NOT NULL,
    StockProducto INT NOT NULL,
    EstadoProducto VARCHAR(20),
    IdCategoria INTEGER NOT NULL,
    FOREIGN KEY(IdCategoria) REFERENCES Categoria(IdCategoria)
);